﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrintServer
{
    public partial class Main : Form
    {
        decimal TotalAmount = 0, NetAmount = 0, Discount = 0, Tax = 0;
        string InvoiceNo; int PrinterSettingsID = 0, CompanyID = 0;
        int i = 0, j = 0;
        ListBox Listing = new ListBox();
        DataGridView SampleDataGrid = new DataGridView();
        PrintDbEntities Db = new PrintDbEntities();

        Bitmap bmp;
        private System.IO.Stream streamToPrint;
        string streamType;
        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern bool BitBlt
     (
         IntPtr hdcDest, // handle to destination DC
         int nXDest, // x-coord of destination upper-left corner
         int nYDest, // y-coord of destination upper-left corner
         int nWidth, // width of destination rectangle
         int nHeight, // height of destination rectangle
         IntPtr hdcSrc, // handle to source DC
         int nXSrc, // x-coordinate of source upper-left corner
         int nYSrc, // y-coordinate of source upper-left corner
         System.Int32 dwRop // raster operation code
     );

        #region Member Variables

        StringFormat strFormat; //Used to format the grid rows.
        ArrayList arrColumnLefts = new ArrayList();//Used to save left coordinates of columns
        ArrayList arrColumnWidths = new ArrayList();//Used to save column widths
        int iCellHeight = 0; //Used to get/set the datagridview cell height
        int iTotalWidth = 0; //
        int iRow = 0;//Used as counter
        bool bFirstPage = false; //Used to check whether we are printing first page
        bool bNewPage = false;// Used to check whether we are printing a new page
        int iHeaderHeight = 0; //Used for the header height
        #endregion

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SampleDataGrid.ColumnCount = 6;
            SampleDataGrid.Columns[0].Name = "No";
            SampleDataGrid.Columns[1].Name = "ProductName";
            SampleDataGrid.Columns[2].Name = "Total";
            SampleDataGrid.Columns[3].Name = "Qty";
            SampleDataGrid.Columns[4].Name = "Tax";
            SampleDataGrid.Columns[5].Name = "NetAmount";

            SampleDataGrid.Columns[0].HeaderText = "No";
            SampleDataGrid.Columns[1].HeaderText = "Produc tName";
            SampleDataGrid.Columns[2].HeaderText = "Total";
            SampleDataGrid.Columns[3].HeaderText = "Qty";
            SampleDataGrid.Columns[4].HeaderText = "Tax";
            SampleDataGrid.Columns[5].HeaderText = "Net Amount";

            SampleDataGrid.AutoResizeColumns();
            SampleDataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            SampleDataGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.SampleDataGrid.AdvancedCellBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            this.SampleDataGrid.AdvancedCellBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            this.SampleDataGrid.AdvancedCellBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            this.SampleDataGrid.AdvancedCellBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
            this.SampleDataGrid.AllowUserToAddRows = false;

            var Listt = Db.PrintTbls.ToList();
            if (Listt != null)
            {

                foreach (var Item in Listt)
                {

                    var PrinterSettings = Db.PrintSettingsTbls.Where(x => x.PrintSettingsID == Item.PrintSettingsID).FirstOrDefault();
                    //if(PrinterSettings.PrinterName=="Default")
                    //{
                    //    PrinterName = "Default";

                    int no = 1;
                    var SalesList = Db.SalesTbls.Where(x => x.SalesID == Item.SalesID).FirstOrDefault();
                    PrinterSettingsID = Convert.ToInt32(Item.PrintSettingsID.ToString());

                    var SalesDetailsList = Db.SalesDetailTbls.Where(x => x.SalesID == Item.SalesID).ToList();
                    foreach (var item in SalesDetailsList)
                    {
                        var ProductList = Db.ProductTbls.Where(x => x.ProductID == item.ProductID).FirstOrDefault();
                        if (PrinterSettings.PrinterType == "Thermal")
                        {
                            Listing.Items.Add(no.ToString().PadRight(2) + ProductList.ProductName.PadRight(12) + item.Total.ToString().PadRight(10) + item.Qty.ToString().PadRight(3) + item.TAX.ToString().PadRight(4) + item.NetAmount);
                        }
                        else if (PrinterSettings.PrinterType == "A4")
                        {
                            SampleDataGrid.Rows.Add(no.ToString(), ProductList.ProductName, item.Total.ToString(), item.Qty.ToString(), item.TAX.ToString(), item.NetAmount);

                        }
                        else
                        {

                        }
                        no++;
                    }
                    InvoiceNo = Item.SalesID.ToString() + "TECH18";
                    TotalAmount = Decimal.Parse(SalesList.TotalAmount.ToString());
                    Tax = Decimal.Parse(SalesList.Tax.ToString());
                    Discount = Decimal.Parse(SalesList.Discount.ToString());
                    NetAmount = Decimal.Parse(SalesList.NetAmount.ToString());
                    CompanyID = Convert.ToInt32(Item.CompanyID.ToString());



                    if (PrinterSettings.PrinterType == "Thermal")
                    {
                        PrintDialog printDialog = new PrintDialog();

                        PrintDocument printDocument = new PrintDocument();

                        printDialog.Document = printDocument; //add the document to the dialog box...        

                        printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt); //add an event handler that will do the printing


                        printDocument.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                        printDocument.Print();


                    }
                    else if (PrinterSettings.PrinterType == "A4")
                    {
                        PrintDialog printDialog = new PrintDialog();

                        PrintDocument printDocument = new PrintDocument();

                        printDialog.Document = printDocument;
                        //string strText = "";
                        //foreach (object x in Listing.Items)
                        //{
                        //    strText = strText + x.ToString() + "\n";

                        //}

                        //myReader = new StringReader(strText);
                        //if (printDialog.ShowDialog() == DialogResult.OK)
                        //{


                        printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateA4Receipt); //add an event handler that will do the printing


                        printDocument.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                        printDocument.Print();
                        //}
                    }
                    else
                    {

                    }

                }
            }
        }

        public void CreateA4Receipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //try
            //{

            strFormat = new StringFormat();
            strFormat.Alignment = StringAlignment.Near;
            strFormat.LineAlignment = StringAlignment.Center;
            strFormat.Trimming = StringTrimming.EllipsisCharacter;

            arrColumnLefts.Clear();
            arrColumnWidths.Clear();
            iCellHeight = 0;
            iRow = 0;
            bFirstPage = true;
            bNewPage = true;
            int offset = 10;
            Font font = new Font("Courier New", 8);
            float fontHeight = font.GetHeight();
            // Calculating Total Widths
            iTotalWidth = 0;
            foreach (DataGridViewColumn dgvGridCol in SampleDataGrid.Columns)
            {
                iTotalWidth += dgvGridCol.Width;
            }


            //Set the left margin
            int iLeftMargin = e.MarginBounds.Left;
            //Set the top margin
            int iTopMargin = e.MarginBounds.Top;
            //Whether more pages have to print or not
            bool bMorePagesToPrint = false;
            int iTmpWidth = 0;

            //For the first page to print set the cell width and header height
            if (bFirstPage)
            {
                foreach (DataGridViewColumn GridCol in SampleDataGrid.Columns)
                {
                    iTmpWidth = (int)(Math.Floor((double)((double)GridCol.Width /
                        (double)iTotalWidth * (double)iTotalWidth *
                        ((double)e.MarginBounds.Width / (double)iTotalWidth))));

                    iHeaderHeight = (int)(e.Graphics.MeasureString(GridCol.HeaderText,
                        GridCol.InheritedStyle.Font, iTmpWidth).Height) + 11;

                    // Save width and height of headers
                    arrColumnLefts.Add(iLeftMargin);
                    arrColumnWidths.Add(iTmpWidth);
                    iLeftMargin += iTmpWidth;
                }
            }
            //Loop till all the grid rows not get printed
            while (iRow <= SampleDataGrid.Rows.Count - 1)
            {
                DataGridViewRow GridRow = SampleDataGrid.Rows[iRow];
                //Set the cell height
                iCellHeight = GridRow.Height + 5;
                int iCount = 0;
                //Check whether the current page settings allows more rows to print
                if (iTopMargin + iCellHeight >= e.MarginBounds.Height + e.MarginBounds.Top)
                {
                    bNewPage = true;
                    bFirstPage = false;
                    bMorePagesToPrint = true;
                    break;
                }
                else
                {
                    if (bNewPage)
                    {
                        //Draw Header
                        e.Graphics.DrawString("Techxora Private Limited",
                            new Font(new Font("Courier New", 14), FontStyle.Bold),
                            Brushes.Black, e.MarginBounds.Left + 200,
                            e.MarginBounds.Top - e.Graphics.MeasureString("Techxora Private Limited",
                            new Font(SampleDataGrid.Font, FontStyle.Bold),
                            e.MarginBounds.Width).Height - 13 + offset);
                        offset = offset + (int)fontHeight + 10;


                        e.Graphics.DrawString("Saugandhigam Building,Thrissur",
                              new Font(new Font("Courier New", 12), FontStyle.Bold),
                              Brushes.Black, e.MarginBounds.Left + 175,
                              e.MarginBounds.Top - e.Graphics.MeasureString("Saugandhigam Building,Thrissur",
                              new Font(SampleDataGrid.Font, FontStyle.Bold),
                              e.MarginBounds.Width).Height - 13 + offset);
                        offset = offset + (int)fontHeight + 10;

                        e.Graphics.DrawString("Ph:0480-27055901,Mob:+91-7685945369",
                           new Font(new Font("Courier New", 12), FontStyle.Bold),
                           Brushes.Black, e.MarginBounds.Left + 150,
                           e.MarginBounds.Top - e.Graphics.MeasureString("Saugandhigam Building,Thrissur",
                           new Font(SampleDataGrid.Font, FontStyle.Bold),
                           e.MarginBounds.Width).Height - 13 + offset);
                        offset = offset + (int)fontHeight + 10;

                        e.Graphics.DrawString("Invoice No:SCTEH234",
                        new Font(new Font("Courier New", 10), FontStyle.Bold),
                        Brushes.Black, e.MarginBounds.Left,
                        e.MarginBounds.Top - e.Graphics.MeasureString("Invoice No:SCTEH234",
                        new Font(SampleDataGrid.Font, FontStyle.Bold),
                        e.MarginBounds.Width).Height - 13 + offset);

                        String strDate = DateTime.Now.ToLongDateString() + " " +
                                DateTime.Now.ToShortTimeString();
                        //Draw Date
                        e.Graphics.DrawString(strDate,
                            new Font(new Font("Courier New", 10), FontStyle.Bold), Brushes.Black,
                            e.MarginBounds.Left +
                            (e.MarginBounds.Width - e.Graphics.MeasureString(strDate,
                            new Font(SampleDataGrid.Font, FontStyle.Bold),
                            e.MarginBounds.Width).Width),
                            e.MarginBounds.Top - e.Graphics.MeasureString("Customer Summary",
                            new Font(new Font(SampleDataGrid.Font, FontStyle.Bold),
                            FontStyle.Bold), e.MarginBounds.Width).Height - 13 + offset);

                        offset = offset + (int)fontHeight + 30;
                        //Draw Columns                 
                        iTopMargin = e.MarginBounds.Top + offset;
                        foreach (DataGridViewColumn GridCol in SampleDataGrid.Columns)
                        {
                            e.Graphics.FillRectangle(new SolidBrush(Color.LightGray),
                                new Rectangle((int)arrColumnLefts[iCount], iTopMargin,
                                (int)arrColumnWidths[iCount], iHeaderHeight));

                            e.Graphics.DrawRectangle(Pens.Black,
                                new Rectangle((int)arrColumnLefts[iCount], iTopMargin,
                                (int)arrColumnWidths[iCount], iHeaderHeight));

                            e.Graphics.DrawString(GridCol.HeaderText,
                                /*GridCol.InheritedStyle.Font*/new Font("Courier New", 10),
                                new SolidBrush(GridCol.InheritedStyle.ForeColor),
                                new RectangleF((int)arrColumnLefts[iCount], iTopMargin,
                                (int)arrColumnWidths[iCount], iHeaderHeight), strFormat);
                            iCount++;
                        }
                        bNewPage = false;
                        iTopMargin += iHeaderHeight;

                        offset = iTopMargin + 50;

                        e.Graphics.DrawString("Total Amount",
                        new Font(new Font("Courier New", 12), FontStyle.Bold),
                        Brushes.Black, e.MarginBounds.Left,
                        e.MarginBounds.Top - e.Graphics.MeasureString("Total Amount",
                        new Font(SampleDataGrid.Font, FontStyle.Bold),
                        e.MarginBounds.Width).Height - 13 + offset);


                        e.Graphics.DrawString("2500",
                            new Font(new Font("Courier New", 12), FontStyle.Bold), Brushes.Black,
                            e.MarginBounds.Left +
                            (e.MarginBounds.Width - e.Graphics.MeasureString(strDate,
                            new Font(SampleDataGrid.Font, FontStyle.Bold),
                            e.MarginBounds.Width).Width),
                            e.MarginBounds.Top - e.Graphics.MeasureString("Total Amount",
                            new Font(new Font(SampleDataGrid.Font, FontStyle.Bold),
                            FontStyle.Bold), e.MarginBounds.Width).Height - 13 + offset);
                        offset = offset + 15;

                        e.Graphics.DrawString("Tax Amount",
                       new Font(new Font("Courier New", 12), FontStyle.Bold),
                       Brushes.Black, e.MarginBounds.Left,
                       e.MarginBounds.Top - e.Graphics.MeasureString("Tax Amount",
                       new Font(SampleDataGrid.Font, FontStyle.Bold),
                       e.MarginBounds.Width).Height - 13 + offset);


                        e.Graphics.DrawString("2500",
                            new Font(new Font("Courier New", 12), FontStyle.Bold), Brushes.Black,
                            e.MarginBounds.Left +
                            (e.MarginBounds.Width - e.Graphics.MeasureString(strDate,
                            new Font(SampleDataGrid.Font, FontStyle.Bold),
                            e.MarginBounds.Width).Width),
                            e.MarginBounds.Top - e.Graphics.MeasureString("Tax Amount",
                            new Font(new Font(SampleDataGrid.Font, FontStyle.Bold),
                            FontStyle.Bold), e.MarginBounds.Width).Height - 13 + offset);
                        offset = offset + 15;

                        e.Graphics.DrawString("Discount Amount",
                       new Font(new Font("Courier New", 12), FontStyle.Bold),
                       Brushes.Black, e.MarginBounds.Left,
                       e.MarginBounds.Top - e.Graphics.MeasureString("Discount Amount",
                       new Font(SampleDataGrid.Font, FontStyle.Bold),
                       e.MarginBounds.Width).Height - 13 + offset);


                        e.Graphics.DrawString("2500",
                            new Font(new Font("Courier New", 12), FontStyle.Bold), Brushes.Black,
                            e.MarginBounds.Left +
                            (e.MarginBounds.Width - e.Graphics.MeasureString(strDate,
                            new Font(SampleDataGrid.Font, FontStyle.Bold),
                            e.MarginBounds.Width).Width),
                            e.MarginBounds.Top - e.Graphics.MeasureString("Discount Amount",
                            new Font(new Font(SampleDataGrid.Font, FontStyle.Bold),
                            FontStyle.Bold), e.MarginBounds.Width).Height - 13 + offset);
                        offset = offset + 30;
                        e.Graphics.DrawString("Net Amount",
                        new Font(new Font("Courier New", 14), FontStyle.Bold),
                        Brushes.Black, e.MarginBounds.Left,
                        e.MarginBounds.Top - e.Graphics.MeasureString("Net Amount",
                        new Font(SampleDataGrid.Font, FontStyle.Bold),
                        e.MarginBounds.Width).Height - 13 + offset);


                        e.Graphics.DrawString("2500",
                            new Font(new Font("Courier New", 14), FontStyle.Bold), Brushes.Black,
                            e.MarginBounds.Left +
                            (e.MarginBounds.Width - e.Graphics.MeasureString(strDate,
                            new Font(SampleDataGrid.Font, FontStyle.Bold),
                            e.MarginBounds.Width).Width),
                            e.MarginBounds.Top - e.Graphics.MeasureString("Net Amount",
                            new Font(new Font(SampleDataGrid.Font, FontStyle.Bold),
                            FontStyle.Bold), e.MarginBounds.Width).Height - 13 + offset);
                    }
                    iCount = 0;
                    //Draw Columns Contents                
                    foreach (DataGridViewCell Cel in GridRow.Cells)
                    {
                        if (Cel.Value != null)
                        {
                            e.Graphics.DrawString(Cel.Value.ToString(),
                              /*  Cel.InheritedStyle.Font*/new Font("Courier New", 10),
                                new SolidBrush(Cel.InheritedStyle.ForeColor),
                                new RectangleF((int)arrColumnLefts[iCount],
                                (float)iTopMargin,
                                (int)arrColumnWidths[iCount], (float)iCellHeight),
                                strFormat);
                        }
                        //Drawing Cells Borders 
                        e.Graphics.DrawRectangle(Pens.Black,
                            new Rectangle((int)arrColumnLefts[iCount], iTopMargin,
                            (int)arrColumnWidths[iCount], iCellHeight));
                        iCount++;
                    }
                }
                iRow++;
                iTopMargin += iCellHeight;
            }
            //If more lines exist, print another page.
            if (bMorePagesToPrint)
                e.HasMorePages = true;
            else
                e.HasMorePages = false;
            //}
            //catch (Exception exc)
            //{
            //    MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK,
            //       MessageBoxIcon.Error);
            //}
         
        }

        public void CreateReceipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {


            var Settings = Db.PrintSettingsTbls.Where(x => x.PrintSettingsID == PrinterSettingsID).FirstOrDefault();
            var companydetails = Db.CompanyTbls.Where(x => x.CompanyID == CompanyID).FirstOrDefault();
            //int total = 0;
            //float cash = float.Parse(txtCash.Text.Substring(1, 5));
            //float change = 0.00f;
            string SubTotal1, IGSTTotal1, IGSTTotal2, DiscountTot, NetAmount1;
            SubTotal1 = "₹" +/*Tot.ToString()*/TotalAmount.ToString();

            IGSTTotal1 = "₹" + Tax.ToString();
            //  IGSTTotal2 = "₹" + TotalTax2.ToString();
            DiscountTot = "₹" + Discount.ToString();
            NetAmount1 = "₹" + NetAmount.ToString();
            // BalanceDue = "₹" + PrintBLL.NetAmount;

            //this prints the reciept

            Graphics graphic = e.Graphics;

            Font font = new Font(Settings.FontName, Convert.ToInt32(Settings.FontSize1)); //must use a mono spaced font as the spaces need to line up
            Font font1 = new Font(Settings.FontName, Convert.ToInt32(Settings.FontSize2), FontStyle.Bold);//must use a mono spaced font as the spaces need to line up
            float fontHeight = font.GetHeight();

            int startX = Convert.ToInt32(Settings.PrintPageMarginLeft);
            int startY = Convert.ToInt32(Settings.PrintPageMarginTop);
            int offset = 10;

            graphic.DrawString("".PadRight(Convert.ToInt32(Settings.HeaderMarginLeft)) + companydetails.CompanyName, new Font(Settings.FontName, Convert.ToInt32(Settings.HeaderFontSize), FontStyle.Bold), new SolidBrush(Color.Black), startX, startY);
            offset = offset + (int)fontHeight + Convert.ToInt32(Settings.CompanyNameOffset);
            graphic.DrawString("".PadRight(Convert.ToInt32(Settings.HeaderMarginLeft)) + companydetails.CompanyAddress, new Font(Settings.FontName, Convert.ToInt32(Settings.AddressFontSize), FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight + Convert.ToInt32(Settings.AddressOffset);
            //graphic.DrawString("   GSTIN : 32AACFK7759A1ZL", new Font("Courier New", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
            //offset = offset + (int)fontHeight + 5;
            DateTime dt = DateTime.Now;

            string top1 = InvoiceNo.PadRight(Convert.ToInt32(Settings.InvoicePadRight)) + dt.ToString("dd/MM/yyyy");

            graphic.DrawString(top1, new Font(Settings.FontName, Convert.ToInt32(Settings.FontSize1)), new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight + 15;
            string top = "No".PadRight(2) + "Item".PadRight(12) + "Rate".PadRight(10) + "Qty".PadRight(5) + "GST" + "%".PadRight(4) + "Total";
            graphic.DrawString(top, font1, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight; //make the spacing consistent
            graphic.DrawString("-------------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight + 5; //make the spacing consistent

            float totalprice = 0.00f;

            foreach (string item in Listing.Items)
            {
                //create the string to print on the reciept
                string productDescription = item;
                string productTotal = item.Substring(item.Length - 6, 6);
                float productPrice = float.Parse(item.Substring(item.Length - 5, 5));

                //MessageBox.Show(item.Substring(item.Length - 5, 5) + "PROD TOTAL: " + productTotal);


                totalprice += productPrice;

                //if (productDescription.Contains("  -"))
                //{
                //    string productLine = productDescription.Substring(0, 24);

                //    graphic.DrawString(productLine, new Font("Courier New", 12, FontStyle.Italic), new SolidBrush(Color.Red), startX, startY + offset);

                //    offset = offset + (int)fontHeight + 5; //make the spacing consistent
                //}
                //else
                //{
                // string productLine = productDescription;

                graphic.DrawString(item, font1, new SolidBrush(Color.Black), startX, startY + offset);

                offset = offset + (int)fontHeight + 5; //make the spacing consistent
                //}

            }

            //change = (cash - totalprice);

            //when we have drawn all of the items add the total

            offset = offset + Convert.ToInt32(Settings.TotalOffset1); //make some room so that the total stands out.

            graphic.DrawString("Total".PadRight(20) + String.Format("{0:c}", SubTotal1), font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + Convert.ToInt32(Settings.TotalOffset2);
            graphic.DrawString("GST".PadRight(20) + String.Format("{0:c}", IGSTTotal1), font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + Convert.ToInt32(Settings.TotalOffset2);
            //graphic.DrawString("GST 12%".PadRight(20) + String.Format("{0:c}", IGSTTotal2), font, new SolidBrush(Color.Black), startX, startY + offset);


            //offset = offset + 15;
            graphic.DrawString("Discount".PadRight(20) + String.Format("{0:c}", DiscountTot), font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 30; //make some room so that the total stands out.

            graphic.DrawString("Net Amount".PadRight(20) + String.Format("{0:c}", NetAmount), new Font(Settings.FontName, Convert.ToInt32(Settings.AddressFontSize), FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);


            offset = offset + 30; //make some room so that the total stands out.
            graphic.DrawString("        Thank-you for !!!", font, new SolidBrush(Color.Black), startX, startY + offset);
            //offset = offset + 15;
            //graphic.DrawString("       please come back soon!", font, new SolidBrush(Color.Black), startX, startY + offset);

        }
    }
}
